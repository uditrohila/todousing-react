import logo from './logo.svg';
import './App.css';
import Header from './components/Header';
import InputItem from './components/InputItem';
import ListItems from './components/ListItems';
import Footer from './components/Footer';

function App() {
  return (
    <div className="App">
    <Header/>
    <InputItem/>
    {/* <Footer/> */}
    {/* <ListItems/> */}
    </div>
  );
}

export default App;
