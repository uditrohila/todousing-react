import React, { Component } from "react";
import crossimg from "../images/icon-cross.svg";
import checkimg from "../images/icon-check.svg";

class ListItems extends Component {

 
 
  render() {
    // console.log(this.props.itemcopy);
    return (
      <section>
        <div className="todo-items-container">
          {/* <!-- all items container --> */}
          <div id="todo-items" className="todo-items">
          


       

            {this.props.itemcopy.map((item)=>{
                return(
                    <div key={item.id} className="todo-item">
              <div className="check">
                <input
                checked={item.completed} 
                type="checkbox" 
                className="check-mark" 
                onClick={() => this.props.togglecheck(item.id)}/>
              </div>
              <div className={(item.completed?"todo-text complete":"todo-text")}>{item.value}</div>
              <button className="close"  onClick={() => this.props.onDelete(item.id)} >
                <img src={crossimg} />
              </button>
            </div>
                )
            })}
          </div>
         
        </div>
      </section>
    );
  }
}

export default ListItems;
