import React, { Component } from "react";
import ListItems from "./ListItems";
import Footer from "./Footer";

class InputItem extends Component {
  state = {
    listitem: [],
    listitemCopy: [],
  };

  handleDelete = (counterId) => {
    console.log(this.state.listitem);
    const counters = this.state.listitemCopy.filter(
      (item) => item.id !== counterId
    );
    this.setState({ listitemCopy: counters });
    this.setState({listitem: counters});

  };

  // handleLine = (Id) => {
  //   const counters = this.state.listitem.map((item) => {
  //     if (item.id !== Id) {
  //       item.activeClass = !item.activeClass;
  //     }
  //     return item;
  //   });
  //   this.setState({ listitem: counters });
  // };

  togglecheck = (toggleId) => {
    let item = this.state.listitem.map((task) => {
      if (task.id == toggleId) {
        task.completed = !task.completed;
      }
      return task;
    });

    this.setState({ listitem: item });
  };

  handleClear = () => {
    let item = this.state.listitem.filter((task) => task.completed == false);
    this.setState({ listitemCopy: item });
  };

  handleallActive = () => {
    let item = this.state.listitem.filter((task) => task.completed == false);
    this.setState({ listitemCopy: item });
  };

  handleAll = () => {
    let item = this.state.listitem;
    this.setState({ listitemCopy: item });
  };

  handleCompleted = () => {
    let item = this.state.listitem.filter((task) => task.completed == true);
    this.setState({ listitemCopy: item });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    // const value = this.state.value;
    let itemValue = e.target.content.value;
    let item = {
      value: itemValue,
      id: Math.random(100),
      completed: false,
      activeClass: true,
    };
    // console.log(item);
    let newListitem = [...this.state.listitem, item];
    // console.log(this.state.listitem,"hello");
    // let newItem=this.state.listitem.push(item);

    this.setState({ listitem: newListitem });
    this.setState({ listitemCopy: newListitem });
    e.target.content.value = "";
    // console.log('Value on submit:', value);

    // let item = {id: new Date(), task: value, completed: false};
    // this.setState({listitem: item});
    // this.setState({listitem : this.state.listitem.push(item)});
    // console.log(this.state.listitem);
  };

  // getValue = (event) => {
  //     // console.log('Event:', event.target.value);

  // }
  render() {
    return (
      <div>
        <section>
          <div className="new-todo">
            {/* check mark div  */}
            <div className="check">
              <div className="check-mark"></div>
            </div>

            {/* todo input div */}
            <div className="new-todo-input">
              <form onSubmit={this.handleSubmit} id="todo-input">
                <input
                  type="text"
                  name="content"
                  placeholder="Create a new todo...."
                  // onChange={ this.getValue}
                />
              </form>
            </div>
          </div>
        </section>
        <ListItems
          items={this.state.listitem}
          onDelete={this.handleDelete}
          line={this.handleLine}
          togglecheck={this.togglecheck}
          itemcopy={this.state.listitemCopy}
        />
        <Footer
          clear={this.handleClear}
          Activeall={this.handleallActive}
          All={this.handleAll}
          Completed={this.handleCompleted}
          totalitems={this.state.listitem.length}
        />
      </div>
    );
  }
}

export default InputItem;
