import React, { Component } from "react";
import sunimg from "../images/icon-sun.svg"
import desktopdark from "../images/bg-desktop-dark.jpg"

class Header extends Component {
  state = {
  };
  render() {
    return (
      <section>
       
        <div className="background-image">
          <img src={desktopdark} />
        </div>

        <div className="main-container">
          <div className="header">
            <div className="title">TODO</div>

            <div className="theme">
              <img src={sunimg} />
            </div>
          </div>
        </div>

      </section>
    );
  }
}

export default Header;
