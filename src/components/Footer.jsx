import React, { Component } from 'react'

class Footer extends Component {
    state = {  } 
    render() { 
        return (
            <footer>
                 <div className="todo-items-info">
            <div className="items-left">
              <span>{this.props.totalitems}</span>items
            </div>
            <div className="items-statuses">
              <span className="active" onClick={this.props.All}>All</span>
              <span  onClick={this.props.Activeall}>Active</span>
              <span onClick={this.props.Completed}>Completed</span>
            </div>

            <div className="items-clear">
              <span onClick={this.props.clear}>Clear Completed</span>
            </div>
          </div>
            </footer>
        );
    }
}
 
export default Footer;